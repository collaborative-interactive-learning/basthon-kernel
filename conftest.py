"""
Various common utilities for testing.
"""

import contextlib
import multiprocessing
import textwrap
import tempfile
import time
import os
import pathlib
import queue
import sys
import shutil
import logging

ROOT_PATH = pathlib.Path(__file__).parent.resolve()
TEST_PATH = ROOT_PATH / "tests"
BUILD_PATH = TEST_PATH / "build"

sys.path.append(str(ROOT_PATH))

import selenium.webdriver.common.utils as _  # noqa: F401 E402


try:
    import pytest

    def pytest_addoption(parser):
        group = parser.getgroup("general")
        group.addoption(
            "--run-xfail",
            action="store_true",
            help="If provided, tests marked as xfail will be run",
        )
except ImportError:
    pytest = None  # type: ignore


def pytest_generate_tests(metafunc):
    # dynamic browser params generation
    browsers = ["firefox", "chrome"]
    if shutil.which("microsoft-edge") is not None:
        browsers.append("edge")
    if 'browser' in metafunc.fixturenames:
        metafunc.parametrize("browser", browsers, scope="session")


class JavascriptException(Exception):
    def __init__(self, msg, stack):
        self.msg = msg
        self.stack = stack
        # In chrome the stack contains the message
        if self.stack and self.stack.startswith(self.msg):
            self.msg = ""

    def __str__(self):
        return "\n\n".join(x for x in [self.msg, self.stack] if x)


class SeleniumWrapper:
    JavascriptException = JavascriptException

    def __init__(
            self,
            server_port,
            query,
            server_hostname="127.0.0.1",
            server_log=None,
    ):
        build_dir = BUILD_PATH

        self.driver = self.get_driver()
        self.server_port = server_port
        self.server_hostname = server_hostname
        self.server_log = server_log
        self.browser_name = self.driver.capabilities.get(
            'browserName', 'unknown')
        self.browser_version = self.driver.capabilities.get(
            'browserVersion', self.driver.capabilities.get(
                'version', 'unknown'))
        logging.info(f"Using browser {self.browser_name} "
                     f"version {self.browser_version}")

        if not (pathlib.Path(build_dir) / 'test.html').exists():
            # selenium does not expose HTTP response codes
            raise ValueError(
                f"{(build_dir / 'test.html').resolve()} does not exist!"
            )
        self.driver.implicitly_wait(120)
        self.driver.set_script_timeout(120)
        self.driver.get(f"http://{server_hostname}:{server_port}/test.html?{query}")
        self.run_js("try { await Basthon.loaded(); } catch(e) {}")
        self.run_js("Error.stackTraceLimit = Infinity")

        # connecting Basthon's test listeners (for run_basthon)
        self.driver.execute_script("""
            Basthon.addEventListener("eval.output", function (data) {
                switch(data.stream) {
                case "stdout":
                    window._basthon_eval_data.stdout += data.content;
                    break;
                case "stderr":
                    window._basthon_eval_data.stderr += data.content;
                    break;
                }
            });

            Basthon.addEventListener("eval.display", function (data) {
                window._basthon_eval_data.display = data;
            });

            function callback (data) {
                window._basthon_eval_data.result = data;
                window._basthon_eval_data.done = true;
                if( window._basthon_done != null )
                    window._basthon_done();
            }

            Basthon.addEventListener("eval.finished", callback);
            Basthon.addEventListener("eval.error", callback);
        """)

    @property
    def logs(self):
        logs = self.driver.execute_script("return window.logs")
        if logs is not None:
            return "\n".join(str(x) for x in logs)
        else:
            return ""

    def clean_logs(self):
        self.driver.execute_script("window.logs = []")

    def run_js(self, code):
        if isinstance(code, str) and code.startswith("\n"):
            # we have a multiline string, fix indentation
            code = textwrap.dedent(code)

        wrapper = """
            let cb = arguments[arguments.length - 1];
            let run = async () => { %s };
            (async () => {
                try {
                    let result = await run();
                    if(window.pyodide && pyodide._module && pyodide._module._PyErr_Occurred()){
                        try {
                            pyodide._module._pythonexc2js();
                        } catch(e){
                            console.error(`Python exited with error flag set! Error was:\n{e.message}`);
                            // Don't put original error message in new one: we want
                            // "pytest.raises(xxx, match=msg)" to fail
                            throw new Error(`Python exited with error flag set!`);
                        }
                    }
                    cb([0, result]);
                } catch (e) {
                    cb([1, e.toString(), e.stack]);
                }
            })();
        """

        retval = self.driver.execute_async_script(wrapper % code)

        if retval[0] == 0:
            return retval[1]
        else:
            raise JavascriptException(retval[1], retval[2])

    def run_basthon_detach(self, code):
        self.driver.execute_script("""
            const code = arguments[0];

            window._basthon_eval_data = {
                result: undefined,
                stdout: "",
                stderr: "",
                display: undefined,
                done: false,
            };

            window._basthon_done = null;
            Basthon.dispatchEvent("eval.request", {code: code});""", code)

    def run_basthon_reattach(self, return_data=True):
        self.driver.execute_async_script("""
            const done = arguments[arguments.length - 1];
            window._basthon_done = done;
            if( window._basthon_eval_data.done )
                done();""")
        if return_data:
            return self.run_js("return window._basthon_eval_data;")

    def run_basthon(self, code, return_data=True):
        self.run_basthon_detach(code)
        return self.run_basthon_reattach(return_data)

    @property
    def urls(self):
        for handle in self.driver.window_handles:
            self.driver.switch_to.window(handle)
            yield self.driver.current_url


class FirefoxWrapper(SeleniumWrapper):

    browser = "firefox"

    def get_driver(self):
        from selenium.webdriver import Firefox
        from selenium.webdriver.firefox.options import Options

        options = Options()
        options.add_argument("-headless")

        return Firefox(executable_path="geckodriver", options=options)


class ChromeWrapper(SeleniumWrapper):

    browser = "chrome"

    def get_driver(self):
        from selenium.webdriver import Chrome
        from selenium.webdriver.chrome.options import Options

        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")

        return Chrome(options=options)


class EdgeWrapper(SeleniumWrapper):

    browser = "edge"

    def get_driver(self):
        from selenium import webdriver

        options = webdriver.EdgeOptions()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")

        return webdriver.Edge(options=options)


if pytest is not None:

    @contextlib.contextmanager
    def _selenium_fixture(web_server_main, browser,
                          language, print_logs=False):
        server_hostname, server_port, server_log = web_server_main
        cls = {"firefox": FirefoxWrapper,
               "chrome": ChromeWrapper,
               "edge": EdgeWrapper}[browser]
        selenium = cls(
            server_port=server_port,
            server_hostname=server_hostname,
            server_log=server_log,
            query=f"kernel={language}",
        )
        try:
            yield selenium
        finally:
            if print_logs:
                print(selenium.logs)
            selenium.driver.quit()

    @pytest.fixture
    def selenium_standalone(web_server_main, browser):
        with _selenium_fixture(web_server_main, browser,
                               "python3", print_logs=True) as s:
            yield s

    @pytest.fixture
    def selenium_standalone_sql(web_server_main, browser):
        with _selenium_fixture(web_server_main, browser,
                               "sql", print_logs=True) as s:
            yield s

    @pytest.fixture
    def selenium_standalone_ocaml(web_server_main, browser):
        with _selenium_fixture(web_server_main, browser,
                               "ocaml", print_logs=True) as s:
            yield s

    @pytest.fixture(scope="session")
    def _selenium_cached(web_server_main, browser):
        with _selenium_fixture(web_server_main, browser,
                               "python3", print_logs=False) as s:
            yield s

    @pytest.fixture(scope="session")
    def _selenium_cached_sql(web_server_main, browser):
        with _selenium_fixture(web_server_main, browser,
                               "sql", print_logs=False) as s:
            yield s

    @pytest.fixture(scope="session")
    def _selenium_cached_ocaml(web_server_main, browser):
        with _selenium_fixture(web_server_main, browser,
                               "ocaml", print_logs=False) as s:
            yield s

    @contextlib.contextmanager
    def _selenium(_selenium_cached):
        # selenium instance cached at the module level
        try:
            _selenium_cached.clean_logs()
            _selenium_cached.run_js("try { Basthon.restart(); } catch(e) {}")
            yield _selenium_cached
        finally:
            print(_selenium_cached.logs)

    @pytest.fixture
    def selenium(_selenium_cached):
        with _selenium(_selenium_cached) as s:
            yield s

    @pytest.fixture
    def selenium_sql(_selenium_cached_sql):
        with _selenium(_selenium_cached_sql) as s:
            yield s

    @pytest.fixture
    def selenium_ocaml(_selenium_cached_ocaml):
        with _selenium(_selenium_cached_ocaml) as s:
            yield s


@pytest.fixture(scope="session")
def web_server_main():
    """Web server that serves files in the build/ directory"""
    with spawn_web_server() as output:
        yield output


@contextlib.contextmanager
def spawn_web_server():
    build_dir = BUILD_PATH

    tmp_dir = tempfile.mkdtemp()
    log_path = pathlib.Path(tmp_dir) / "http-server.log"
    q = multiprocessing.Queue()
    p = multiprocessing.Process(target=run_web_server, args=(q, log_path, build_dir))

    try:
        p.start()
        port = q.get()
        hostname = "127.0.0.1"

        print(
            f"Spawning webserver at http://{hostname}:{port} "
            f"(see logs in {log_path})"
        )
        yield hostname, port, log_path
    finally:
        q.put("TERMINATE")
        p.join()
        shutil.rmtree(tmp_dir)


def run_web_server(q, log_filepath, build_dir):
    """Start the HTTP web server

    Parameters
    ----------
    q : Queue
      communication queue
    log_path : pathlib.Path
      path to the file where to store the logs
    """
    import http.server
    import socketserver

    os.chdir(build_dir)

    log_fh = log_filepath.open("w", buffering=1)
    sys.stdout = log_fh
    sys.stderr = log_fh

    test_prefix = "/src/tests/"

    class Handler(http.server.SimpleHTTPRequestHandler):
        def log_message(self, format_, *args):
            print(
                "[%s] source: %s:%s - %s"
                % (self.log_date_time_string(), *self.client_address, format_ % args)
            )

        def end_headers(self):
            # Enable Cross-Origin Resource Sharing (CORS)
            self.send_header("Access-Control-Allow-Origin", "*")
            super().end_headers()

    with socketserver.TCPServer(("", 0), Handler) as httpd:
        host, port = httpd.server_address
        print(f"Starting webserver at http://{host}:{port}")
        httpd.server_name = "test-server"
        httpd.server_port = port
        q.put(port)

        def service_actions():
            try:
                if q.get(False) == "TERMINATE":
                    print("Stopping server...")
                    sys.exit(0)
            except queue.Empty:
                pass

        httpd.service_actions = service_actions
        httpd.serve_forever()


if (
    __name__ == "__main__"
    and multiprocessing.current_process().name == "MainProcess"
    and not hasattr(sys, "_pytest_session")
):
    with spawn_web_server():
        # run forever
        while True:
            time.sleep(1)
