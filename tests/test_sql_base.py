def test_input_file(selenium_sql):
    input_file = """
DROP TABLE IF EXISTS voitures;
CREATE TABLE voitures (id integer, nom text, marque text);

INSERT INTO voitures VALUES (1,'Kangoo','Renault');
INSERT INTO voitures VALUES (3,'Alpine','Renault');
INSERT INTO voitures VALUES (4,'Golf','Volkswagen');
INSERT INTO voitures VALUES (2,'Mégane','Renault');
INSERT INTO voitures VALUES (5,'Fiesta','Ford');
INSERT INTO voitures VALUES (6,'Polo','Volkswagen');"""
    input_file = [int(b) for b in input_file.encode()]
    selenium_sql.run_js(f"Basthon.putModule('voitures.sql', {str(input_file)});")

    data = selenium_sql.run_basthon("SELECT * FROM voitures;")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == 'id\tnom\tmarque\n1\tKangoo\tRenault\n3\tAlpine\tRenault\n4\tGolf\tVolkswagen\n2\tMégane\tRenault\n5\tFiesta\tFord\n6\tPolo\tVolkswagen'
    
    data = selenium_sql.run_basthon("""
CREATE TABLE constructeurs (marque text, pays text);

INSERT INTO constructeurs VALUES ('Renault', 'France');
INSERT INTO constructeurs VALUES ('Fiat','Italie');
INSERT INTO constructeurs VALUES ('Volkswagen','Allemagne');
INSERT INTO constructeurs VALUES ('Ford','USA');
INSERT INTO constructeurs VALUES ('Ferrari','Italie');""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 2
    assert 'result' not in result

    data = selenium_sql.run_basthon("""
SELECT * FROM constructeurs
WHERE pays like "Italie";""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 3
    result = result['result']
    assert result['text/plain'] == 'marque\tpays\nFiat\tItalie\nFerrari\tItalie'

    data = selenium_sql.run_basthon("""
SELECT nom,v.marque,pays FROM voitures as v
JOIN constructeurs as c on c.marque=v.marque;""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 4
    result = result['result']
    assert result['text/plain'] == 'nom\tmarque\tpays\nKangoo\tRenault\tFrance\nAlpine\tRenault\tFrance\nGolf\tVolkswagen\tAllemagne\nMégane\tRenault\tFrance\nFiesta\tFord\tUSA\nPolo\tVolkswagen\tAllemagne'

    data = selenium_sql.run_basthon("""
UPDATE constructeurs set pays="États Unis" where pays = "USA";
SELECT nom,v.marque,pays FROM voitures as v
JOIN constructeurs as c on c.marque=v.marque;""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 5
    result = result['result']
    assert result['text/plain'] == 'nom\tmarque\tpays\nKangoo\tRenault\tFrance\nAlpine\tRenault\tFrance\nGolf\tVolkswagen\tAllemagne\nMégane\tRenault\tFrance\nFiesta\tFord\tÉtats Unis\nPolo\tVolkswagen\tAllemagne'

    data = selenium_sql.run_basthon(".help;;;\n")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 6
    result = result['result']
    assert result['text/plain'] == """\
.backup\t\tDownload as binary file
.dump\t\tDownload as SQL file
.export\t\tSee .backup
.help\t\tShow this help
.save\t\tSee .backup
.schema\t\tShow the CREATE statements
.tables\t\tList names of tables"""

    data = selenium_sql.run_basthon(".schema")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 7
    result = result['result']
    assert result['text/plain'] == 'sql\nCREATE TABLE voitures (id integer, nom text, marque text)\nCREATE TABLE constructeurs (marque text, pays text)'

    data = selenium_sql.run_basthon(".tables")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 8
    result = result['result']
    assert result['text/plain'] == 'name\nconstructeurs\nvoitures'

    data = selenium_sql.run_js("return Basthon.dump();")
    assert data == """\
CREATE TABLE voitures (id integer, nom text, marque text);
CREATE TABLE constructeurs (marque text, pays text);
INSERT INTO voitures VALUES (1, "Kangoo", "Renault"),
(3, "Alpine", "Renault"),
(4, "Golf", "Volkswagen"),
(2, "Mégane", "Renault"),
(5, "Fiesta", "Ford"),
(6, "Polo", "Volkswagen");
INSERT INTO constructeurs VALUES ("Renault", "France"),
("Fiat", "Italie"),
("Volkswagen", "Allemagne"),
("Ford", "États Unis"),
("Ferrari", "Italie");"""


def test_persistent_input_file(selenium_sql):
    data = selenium_sql.run_basthon("SELECT * FROM voitures;")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == 'id\tnom\tmarque\n1\tKangoo\tRenault\n3\tAlpine\tRenault\n4\tGolf\tVolkswagen\n2\tMégane\tRenault\n5\tFiesta\tFord\n6\tPolo\tVolkswagen'


def test_foreign_key(selenium_sql):
    data = selenium_sql.run_basthon("""
CREATE TABLE artist(
  artistid    INTEGER PRIMARY KEY,
  artistname  TEXT
);

CREATE TABLE track(
  trackid     INTEGER,
  trackname   TEXT,
  trackartist INTEGER,
  FOREIGN KEY(trackartist) REFERENCES artist(artistid)
);

INSERT INTO artist values (1, "Dean Martin"), (2, "Frank Sinatra");
INSERT INTO track values (11, "That's Amore", 1), (12, "Christmas Blues", 1), (13, "My Way", 2);

SELECT * FROM artist;
""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == "artistid\tartistname\n1\tDean Martin\n2\tFrank Sinatra"
    assert result['text/html'] == '<table class="dataframe" border="1"><thead><tr style="text-align: right;"><th>artistid</th><th>artistname</th></tr></thead><tbody><tr><td>1</td><td>Dean Martin</td></tr><tr><td>2</td><td>Frank Sinatra</td></tr></tbody></table>'

    data = selenium_sql.run_basthon("SELECT * FROM track;")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 2
    result = result['result']
    assert result['text/plain'] == "trackid\ttrackname\ttrackartist\n11\tThat's Amore\t1\n12\tChristmas Blues\t1\n13\tMy Way\t2"
    assert result['text/html'] == '<table class="dataframe" border="1"><thead><tr style="text-align: right;"><th>trackid</th><th>trackname</th><th>trackartist</th></tr></thead><tbody><tr><td>11</td><td>That\'s Amore</td><td>1</td></tr><tr><td>12</td><td>Christmas Blues</td><td>1</td></tr><tr><td>13</td><td>My Way</td><td>2</td></tr></tbody></table>'

    data = selenium_sql.run_basthon("INSERT INTO track VALUES(14, 'Mr. Bojangles', 3);")
    assert data['result']['execution_count'] == 3
    assert data['stdout'] == ""
    assert data['stderr'] == "Error: FOREIGN KEY constraint failed"
