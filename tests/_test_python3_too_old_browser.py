from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import selenium.webdriver.support.ui as ui


def test_main(selenium):
    """
    We test the presence of an error message for old version of Firefox.
    We test against FF 56 since it is the first version to support headless
    selenium.
    """
    driver = selenium.driver
    browser = driver.capabilities['browserName']
    if browser != "firefox": return
    xpath = ("//div[contains(., 'Erreur de chargement de Basthon !!!') and contains(., 'Vérifiez que votre navigateur est à jour.') and contains(., 'Version détectée : Firefox 56.')]")
    query = (By.XPATH, xpath)
    wait = ui.WebDriverWait(driver, 10)
    wait.until(ec.visibility_of_element_located(query))
