def test_put_file(selenium):
    selenium.run_js("""
    window.toBytesArray = function(string) {
        string = unescape(encodeURIComponent(string));
        const arr = [];
        for (var i = 0; i < string.length; i++) {
            arr.push(string.charCodeAt(i));
        }
        return Uint8ClampedArray.from(arr);
    }""")

    content = 'hello\n world! ¥£€$¢₡₢₣₤₥₦₧₨₩₪₫₭₮₯₹'
    selenium.driver.execute_script("Basthon.putFile('foo.txt', toBytesArray(arguments[0]))", content)
    data = selenium.run_basthon("""
    with open('foo.txt') as f:
        print(f.read(), end='', flush=True)""")
    assert data['stderr'] == ""
    assert data['stdout'] == content


def test_put_module(selenium):
    assert not selenium.run_js("return Basthon.userModules();")
    content = "foo = 42"
    selenium.driver.execute_async_script("""
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('bar.py', content).then(done);""", content)
    data = selenium.run_basthon("""
    import bar
    print(bar.foo, end='', flush=True)""")
    assert data['stderr'] == ""
    result = data['stdout']
    assert result == "42"
    assert selenium.run_js("return Basthon.userModules();") == ["bar.py"]

    # with a second module

    content = "bar = 24"
    selenium.driver.execute_async_script("""
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('foo.py', content).then(done);""", content)
    data = selenium.run_basthon("""
    import foo
    foo.bar""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']['result']['text/plain']
    assert result == "24"
    assert set(selenium.run_js("return Basthon.userModules();")) == set(["bar.py", "foo.py"])
