from pathlib import Path
import base64
import time
from urllib.parse import quote
import re
from utils import read_and_backup


_test_data = Path(__file__).parent / "data"


def test_all(selenium):
    # ensure all basthon modules are tested
    tested = set(g[len('test_'):] for g in globals()
                 if g.startswith('test_') and g != 'test_all')
    data = selenium.run_basthon("""
    from basthon import packages
    packages._internal_pkgs""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    internal = eval(result['result']['text/plain'])
    assert tested == internal


def test_turtle(selenium):
    # already tested in test_patched_modules
    assert True


def test_branca(selenium):
    # tested through folium
    assert True


def test_folium(selenium):
    # already tested in test_patched_modules
    assert True


def test_graphviz(selenium):
    result = selenium.run_basthon("""
    from graphviz import Digraph
    import basthon

    g = Digraph('G')
    g.edge('Hello', 'World')
    basthon.display(g)
    print(g.source)
    """)
    assert result['stderr'] == ""
    assert result['stdout'] == "digraph G {\n\tHello -> World\n}\n"
    assert result['display']['display_type'] == 'multiple'
    svg = result['display']['content']['image/svg+xml']
    target = read_and_backup(_test_data / "graphviz.svg", svg)
    assert target == svg


def test_requests(selenium):
    result = selenium.run_basthon("""
    import requests

    url = "http://httpbin.org/html"
    response = requests.get(url)
    print(response.text)
    response.headers
    """)
    assert result['stderr'] == ""
    request = result['stdout']
    target = read_and_backup(_test_data / "requests.txt", request)
    assert target == request
    headers = eval(result['result']['result']['text/plain'])
    assert headers['content-type'] == 'text/html; charset=utf-8'


def test_proj4py(selenium):
    result = selenium.run_basthon("""
    import proj4py

    # from WGS84 to Lambert93
    proj = proj4py.proj4('EPSG:4326', 'EPSG:2154')
    proj.forward((46.57824382381372, 2.468613626422624))
    """)
    assert result['stderr'] == ""
    assert result['stdout'] == ""
    target = eval(result['result']['result']['text/plain'])
    assert target == (659306.8946611215, 6608826.400123728)


def test_IPython(selenium):
    result = selenium.run_basthon("""
    import IPython
    import IPython.display
    from IPython.display import display, display_image, IFrame, Markdown
    Markdown("$\\sqrt{2}$")
    """)
    assert result['stderr'] == ""
    assert result['stdout'] == ""
    result = result['result']
    result['result']['text/markdown'] == '$\\sqrt{2}$'


def test_p5(selenium):
    selenium.run_basthon("""
    from p5 import *

    x = 100
    y = 100

    def setup():
        createCanvas(200, 200)

    def draw():
        background(0)
        fill(255)
        rect(x, y, 50, 50)

    run()
    """, return_data=False)
    basename = f"p5_{selenium.driver.capabilities['browserName']}"
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium.run_js("return window._basthon_eval_data.display.content.outerHTML")
    target = read_and_backup(_test_data / f"{basename}.html", html)
    assert html == target
    selenium.run_js("document.body.appendChild(window._basthon_eval_data.display.content);")
    # this should be replaced with a clean selenium wait
    time.sleep(1)
    png = selenium.run_js("""
    const elem = window._basthon_eval_data.display.content;
    return elem.getElementsByTagName('canvas')[0].toDataURL('image/png');
    """)
    png = base64.b64decode(png[len('data:image/png;base64,'):])
    target = read_and_backup(_test_data / f"{basename}.png", png, binary=True)
    target_old = read_and_backup(_test_data / f"{basename}_old.png", png, binary=True)
    target_recent = read_and_backup(_test_data / f"{basename}_recent.png", png, binary=True)
    assert png in (target, target_old, target_recent)

    selenium.run_basthon("stop()")


def test_tutor(selenium):
    result = selenium.run_basthon("""
from tutor import tutor  # with a comment here it should work

a = 5
a = a + 1

tutor()""")
    assert result['stderr'] == ""
    assert result['stdout'] == ""
    assert result['display']['display_type'] == 'tutor'
    assert result['display']['iframe-id'] == 'basthon-pythontutor-iframe-0'
    # webserver starts at arbitrary port so this makes absolute paths
    # inconsistent between calls...
    # we remove the host:port part from URLs
    iframe = result['display']['content']
    basthon_root = selenium.run_js("return Basthon.basthonRoot(true);")
    iframe = iframe.replace(quote(basthon_root), '')
    target = read_and_backup(_test_data / "tutor-iframe.html", iframe)
    assert target == iframe


def test__ivp(selenium):
    # this is tested in test_patched_modules.test_scipy via odeint
    pass


def test_qrcode(selenium):
    # already tested in test_patched_modules
    assert True


def test_osmiter(selenium):
    # already tested in test_patched_modules
    assert True


def test_pyroutelib3(selenium):
    # already tested in test_patched_modules
    assert True


def test_ipythonblocks(selenium):
    result = selenium.run_basthon("""
from ipythonblocks import BlockGrid

w = h = 10

grid = BlockGrid(w, h, block_size=4)

for block in grid:
    block.red = 255 * float(w - block.col) / w
    block.green = 255 * float(h - block.row) / h
    block.blue = 255 * block.col / w

grid.to_text()
grid.show_image()
""")
    assert 'result' not in result['result']
    assert result['stderr'] == ""
    text = result['stdout']
    target = read_and_backup(_test_data / "ipythonblocks_to_text.txt", text)
    assert text == target
    assert result['display']['display_type'] == 'multiple'
    png = base64.b64decode(result['display']['content']['image/png'])
    target = read_and_backup(_test_data / "ipythonblocks_save_image.png", png, binary=True)
    assert png == target


def test_lolviz(selenium):
    result = selenium.run_basthon("""
import lolviz

ma_liste = ['hi', 'mom', {3, 4}, {"parrt":"user"}]

o = lolviz.listviz(ma_liste)
print(o.source)
o
""")
    assert result['stderr'] == ""
    gv = re.sub('node[0-9]+', 'node', result['stdout'])
    target = read_and_backup(_test_data / "lolviz_source.gv", gv)
    assert gv == target
    svg = re.sub('node[0-9]+', 'node',
                 result['result']['result']['image/svg+xml'])
    target = read_and_backup(_test_data / "lolviz.svg", svg)
    assert svg == target


def test_binarytree(selenium):
    # already tested in test_patched_modules
    assert True


def test_rcviz(selenium):
    from ast import literal_eval
    result = selenium.run_basthon("""
from rcviz import viz
@viz
def fibo(n):
    if n < 2:
        return n
    return fibo(n - 1) + fibo(n - 2)

print(fibo(4))
fibo.callgraph()
print(fibo(5))
fibo.callgraph().source
""")
    assert result['stderr'] == ""
    assert result['stdout'] == "3\n5\n"
    gv = literal_eval(result['result']['result']['text/plain'])
    gv = re.sub(r'[0-9]+ -> [0-9]+ \[label=', ' ->  [label=', gv)
    gv = re.sub(r'[0-9]+ \[label=', ' [label=', gv)
    target = read_and_backup(_test_data / "rcviz.gv", gv)
    assert gv == target
