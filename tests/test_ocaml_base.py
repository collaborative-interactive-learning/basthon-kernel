from pathlib import Path
import base64
from utils import read_and_backup


_test_data = Path(__file__).parent / "data"


def test_base(selenium_ocaml):
    # result
    data = selenium_ocaml.run_basthon("1+1")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == "- : int = 2"

    # stdout (1)
    data = selenium_ocaml.run_basthon('print_string "hello world!"')
    assert data['stdout'] == "hello world!"
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 2
    result = result['result']
    assert result['text/plain'] == "- : unit = ()"

    # stdout (2)
    data = selenium_ocaml.run_basthon('print_endline "hello world!"')
    assert data['stdout'] == "hello world!\n"
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 3
    result = result['result']
    assert result['text/plain'] == "- : unit = ()"

    # stderr
    data = selenium_ocaml.run_basthon("1+")
    assert data['stdout'] == ""
    assert data['stderr'] == "Line 1, characters 2-4:\nError: Syntax error\n"
    result = data['result']
    assert result['execution_count'] == 4
    assert 'result' not in result


def test_put_file(selenium_ocaml):
    selenium_ocaml.run_js("""
    window.toBytesArray = function(string) {
        string = unescape(encodeURIComponent(string));
        const arr = [];
        for (var i = 0; i < string.length; i++) {
            arr.push(string.charCodeAt(i));
        }
        return Uint8ClampedArray.from(arr);
    }""")

    content = 'hello\n world! ¥£€$¢₡₢₣₤₥₦₧₨₩₪₫₭₮₯₹'
    selenium_ocaml.driver.execute_script("Basthon.putFile('foo.txt', toBytesArray(arguments[0]))", content)
    data = selenium_ocaml.run_basthon("""
let ch = open_in "foo.txt" in
  let s = really_input_string ch (in_channel_length ch) in
    close_in ch;
    print_string s""")
    assert data['stdout'] == content
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == "- : unit = ()"


def test_put_module(selenium_ocaml):
    content = 'let a = 12\nlet b = 15'
    selenium_ocaml.driver.execute_script("Basthon.putModule('bar.ml', toBytesArray(arguments[0]))", content)
    data = selenium_ocaml.run_basthon("Bar.a + Bar.b")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == "- : int = 27"


def test_basthon_module(selenium_ocaml):
    data = selenium_ocaml.run_basthon('Basthon.version ()')
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == '- : string = "0.0.1"'


def test_canvas(selenium_ocaml):
    selenium_ocaml.run_basthon("""
let canvas = Basthon.create_canvas () in
  Graphics_js.open_canvas canvas;
  Graphics_js.resize_window 200 300;
  Graphics_js.set_color Graphics_js.red;
  Graphics_js.fill_circle 100 150 20;
  Basthon.display_canvas canvas""", return_data=False)
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium_ocaml.run_js("return window._basthon_eval_data.display.content.outerHTML")
    assert html == '<canvas width="200" height="300"></canvas>'
    data = selenium_ocaml.run_js("return JSON.stringify(window._basthon_eval_data.display)")
    assert data == '{"code":"\\nlet canvas = Basthon.create_canvas () in\\n  Graphics_js.open_canvas canvas;\\n  Graphics_js.resize_window 200 300;\\n  Graphics_js.set_color Graphics_js.red;\\n  Graphics_js.fill_circle 100 150 20;\\n  Basthon.display_canvas canvas","interactive":true,"display_type":"ocaml-canvas","content":{}}'


def test_save_canvas(selenium_ocaml):
    browser = selenium_ocaml.driver.capabilities['browserName']
    data = selenium_ocaml.run_basthon("""
let canvas = Basthon.create_canvas () in
  Graphics_js.open_canvas canvas;
  Graphics_js.resize_window 200 200;
  Graphics_js.set_color Graphics_js.red;
  Graphics_js.fill_circle 100 100 20;
  Basthon.save_canvas canvas "ball.png" """)
    # we should wait for file creation
    import time
    time.sleep(5)
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == '- : unit = ()'
    selenium_ocaml.run_basthon('Basthon.display_image "ball.png"', return_data=False)
    # we should wait for display event trigger
    time.sleep(5)
    data = selenium_ocaml.run_js("return window._basthon_eval_data")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 2
    result = result['result']
    assert result['text/plain'] == '- : unit = ()'
    display = data['display']
    assert display['display_type'] == "multiple"
    content = display['content']['image/png']
    png = base64.b64decode(content)
    target = read_and_backup(_test_data / f"ocaml-ball_{browser}.png", png, binary=True)
    target_old = read_and_backup(_test_data / f"ocaml-ball_{browser}_old.png", png, binary=True)
    target_recent = read_and_backup(_test_data / f"ocaml-ball_{browser}_recent.png", png, binary=True)
    assert png in (target, target_old, target_recent)


def test_download_canvas(selenium_ocaml):
    data = selenium_ocaml.run_basthon("""
open Graphics_js;;

let n = 400 and p = 400 and canvas = Basthon.create_canvas () in
  open_canvas canvas;
  resize_window n p;
  let img_array = Array.make n (Array.make 0 black) in
    for i=0 to n-1 do
      img_array.(i) <- Array.make p black;
      for j=0 to p-1 do
        let r = (255 * (p-1 - j)) / (p-1)
        and g = (255 * (n-1 - i)) / (n-1)
        and b = (255 * j) / (p-1) in
          img_array.(i).(j) <- rgb r g b;
      done;
    done;
    let img = make_image img_array in
      draw_image img 0 0;
      Basthon.download_canvas canvas ~format:"jpg" """)
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == '- : unit = ()'


def test_display_image(selenium_ocaml):
    with open(_test_data / "ocaml-ball.png", 'rb') as f:
        content = f.read()
    selenium_ocaml.driver.execute_script(
        "Basthon.putFile('ocaml-ball.png', Uint8ClampedArray.from(arguments[0]))",
        [int(b) for b in content])
    data = selenium_ocaml.run_basthon('Basthon.display_image "ocaml-ball.png"')
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    result = data['result']
    assert result['execution_count'] == 1
    result = result['result']
    assert result['text/plain'] == '- : unit = ()'
    display = data['display']
    assert display['display_type'] == "multiple"
    dataURL = f"{base64.b64encode(content).decode()}"
    assert display['content'] == {'image/png': dataURL}
