from pathlib import Path
import json
from utils import read_and_backup


def test_globals(selenium):
    # should be placed after test_setup but pyodide.version() puts
    # `pyodide` in globale namespace
    data = selenium.run_basthon("import __main__; sorted(dir(__main__))")
    assert data['result']['result']['text/plain'] == str(sorted(['In', 'Out', '_', '__', '___', '__builtins__', '__doc__', '__eval_data__', '__main__', '__name__']))
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    data = selenium.run_basthon("a = 5")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    data = selenium.run_basthon("a")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    assert data['result']['result']['text/plain'] == "5"


def test_setup(selenium):
    assert selenium.run_js("return pyodide.version()") == "0.16.1"
    assert selenium.run_js("return Basthon.pythonVersion") == "3.8.2"


def test_result(selenium):
    for i in range(10):
        data = selenium.run_basthon(f"{i} + {i}")
        assert data['stdout'] == ""
        assert data['stderr'] == ""
        assert data['result']['result']['text/plain'] == str(2 * i)
        assert data['result']['execution_count'] == i + 1


def test_streams(selenium):
    data = selenium.run_basthon("print('foo')")
    assert data['stdout'] == "foo\n" and data['stderr'] == ""
    data = selenium.run_basthon("import sys ; print('bar', file=sys.stderr)")
    assert data['stdout'] == "" and data['stderr'] == "bar\n"


def test_errors(selenium):
    data = selenium.run_basthon("1:")
    assert data['stderr'] == '  File "<input>", line 1\n    1:\n     ^\nSyntaxError: invalid syntax\n'
    assert 'result' not in data['result'] and data['stdout'] == ""
    data = selenium.run_basthon("1 / 0")
    assert data['stderr'] == 'Traceback (most recent call last):\n  File "<input>", line 1, in <module>\nZeroDivisionError: division by zero\n'
    assert 'result' not in data['result'] and data['stdout'] == ""


def test_flush(selenium):
    # flushing should be performed even if not forced
    data = selenium.run_basthon("print('foo', end='bar')")
    assert data['stdout'] == 'foobar'

def test_importables(selenium):
    result = selenium.run_basthon("from basthon import kernel ; kernel.importables()")['result']
    importables = eval(result['result']['text/plain'])

    target = read_and_backup(Path(__file__).parent / "data" / "importables.json",
                             json.dumps(importables))
    target = json.loads(target)
    assert importables == target


def test_import_not_hacked(selenium):
    data = selenium.run_basthon("import pytest; pytest.__version__")
    assert data['result']['result']['text/plain'] == "'3.6.3'"
    assert data['stdout'] == ""
    assert data['stderr'] == ""


def test_banner(selenium):
    banner = selenium.run_js("return Basthon.banner();")
    assert banner.startswith("Python 3.")
    assert 'Type "help", "copyright", "credits" or "license" for more information' in banner
    assert selenium.run_js("return Basthon.banner() === Basthon.__kernel__.banner();") == True


def test_more(selenium):
    tests = [
        ("for i in range(10)", False),
        ("for i in range(10):", True),
        ("for i in range(10):\\n    print(i)", False),
        ("def f(x)", False),
        ("def f(x):", True),
        ("def f(x):\\n    return 2 * x + 1", False),
    ]
    for t, v in tests:
        assert selenium.run_js(f"""return Basthon.more("{t}");""") == v
