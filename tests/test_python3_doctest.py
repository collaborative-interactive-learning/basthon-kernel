def test_globals(selenium):
    # this ensure ou __main__ is properly connected to globals()
    # since doctest.testmod will look in __main__
    selenium.run_basthon("""
import doctest

def my_test(x):
    '''
    >>> my_test(2)
    4
    >>> my_test(4)
    15
    '''
    return x ** 2
""")
    data = selenium.run_basthon("str(doctest.testmod(verbose=False))")
    assert data['stderr'] == ""
    assert data['result']['result']['text/plain'] == "'TestResults(failed=1, attempted=2)'"
