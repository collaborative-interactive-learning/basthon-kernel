# Basthon {#basthon}

## Que signifie "Basthon" ? {#mean}

[Basthon](https://basthon.fr) est l'acronyme de "Bac À Sable pour pyTHON". Il ressemble au mot "baston", c'est une allusion à la "lutte" que peut parfois représenter l'apprentissage de la programmation, l'écriture d'un code ou son débogage.

## Basthon, qu'est-ce que c'est ? {#what-is-it}

C'est :

  - un noyau, [Basthon-Kernel](https://framagit.org/basthon/basthon-kernel) qui s'occupe essentiellement d'interpréter le code (Python, Javascript, SQL, OCaml, Bash) de l'utilisateur, son code source se trouve [ici](https://framagit.org/basthon/basthon-kernel/)
  - des interfaces (frontends) :
    - une interface de type "console", [Basthon-Console](https://console.basthon.fr), dont le code source se trouve [ici](https://framagit.org/basthon/basthon-console/)
    - une interface basée sur Jupyter (classic) Notebook, [Basthon-Notebook](https://notebook.basthon.fr), dont le code source se trouve [ici](https://framagit.org/basthon/basthon-notebook/)
    - une interface (expérimentale) basée sur JupyterLab, [Basthon-JupyterLab](https://jupyterlab.basthon.fr), dont le code source se trouve [ici](https://framagit.org/basthon/basthon-jupyterlab/).
  - une version hors ligne, [Basthon-Desktop](https://basthon.fr/desktop.html), dont le code source se trouve [ici](https://framagit.org/basthon/basthon-desktop/)

Les interfaces sont accessibles [ici](https://basthon.fr).

## À quoi ça sert ? {#used-for}

Basthon est utilisé pour s'initier au language de programmation [Python 3](https://www.python.org) sans rien avoir à installer. Il faut seulement disposer d'un navigateur (Firefox, Chrome/Chromium ou Edge) à jour et d'une connexion à Internet.

## Comment Basthon fonctionne-t-il ? {#how-works}

Le code est intégralement éxécuté par votre navigateur, côté client donc, il n'est même pas transmis sur le réseau. Basthon a été spécialement conçu pour respecter la vie-privée de ses utilisateurs.

Vous pouvez consulter la [documentation de Basthon ici](https://basthon.fr/doc.html).

## Sous quelle licence est placé le code source ? {#license}

Basthon a été intégralement construit à l'aide de logiciels libres. Il est lui-même placé sous la licence libre [GNU GPL version 3](https://www.gnu.org/licenses/quick-guide-gplv3.html) ou toute version ultérieure. Si vous pensez qu'un logiciel libre est simplement un logiciel gratuit, lisez [ceci](https://www.gnu.org/philosophy/free-sw.html). L'ensemble du code source est disponible sur la plateforme [FramaGit](https://framagit.org), [ici](https://framagit.org/basthon/basthon-kernel), [là](https://framagit.org/basthon/basthon-console), [là](https://framagit.org/basthon/basthon-notebook) et [là](https://framagit.org/basthon/basthon-jupyerlab). N'hésitez pas à utiliser le système de suivi des bogues. La [documentation](https://basthon.fr/doc.html) est placé sous la licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

## Quels sont les logiciels et ressources tierces utilisées ? {#third-party}
      
L'exécution du code Python est confiée à [Pyodide](https://github.com/iodide-project/pyodide), une compilation de l'interprète de référence de Python (CPython) en [WebAssembly](https://webassembly.org/). Quelques modifications ont été apportées pour pouvoir utiliser entre autres [P5.js](https://p5js.org), [Matplotlib](https://matplotlib.org/), [Sympy](https://www.sympy.org), [Pandas](https://pandas.pydata.org/), [Turtle](https://docs.python.org/fr/3/library/turtle.html), [Folium](https://python-visualization.github.io/folium/), [Requests](https://requests.readthedocs.io/en/master/), [Proj4js](http://proj4js.org/), [Graphviz](https://graphviz.org/) via le module [graphviz](https://github.com/xflr6/graphviz) à l'aide de [viz.js](https://github.com/mdaines/viz.js/) et [PythonTutor](https://pythontutor.com). Turtle n'est pas implémenté dans Pyodide, c'est une version modifiée de l'implémentation de [Brython](https://brython.info/) qui est utilisée dans Basthon.

Basthon-Notebook est un fork (une copie) de [Jupyter-Notebook](https://github.com/jupyter/notebook). Une grande partie du code derrière l'interface notebook de Basthon provient de Jupyter. C'est seulement l'interface avec Basthon-Kernel qui a été écrite par l'auteur de Basthon.

Basthon-JupyterLab est un fork (une copie) de [JupyterLite](https://github.com/jtpio/jupyterlite).

Le logo Basthon a été réalisé avec la police [KeyTabMetal publié par Tension Type sur dafont.com](https://www.dafont.com/fr/key-tab-metal.font?text=basthon). Les icônes utilisés sont celles de la police [Font Awesome](https://fontawesome.com) et le favicon est le [logo Python](https://www.python.org/community/logos/). Le système de notification et de dialogue de la console est basé sur [notie.js](https://github.com/jaredreich/notie).

## En quoi Basthon respecte-t-il votre vie privée ? {#privacy}

Aucune donnée personnelle enregistrée, aucune publicité, aucune inscription. En effet, aucune donnée personnelle n'est transmise sur le réseau (pas même le code que vous écrivez), aucune donnée statistique n'est donc compilée (pas même sur la fréquentation du site). Aucun cookie n'est enregistré, seuls le dernier code entré et l'apparence de l'affichage sont sauvegardés dans l'espace de stockage local de votre navigateur. Ceci permet de retrouver Basthon dans l'état dans lequel on l'a laissé après une panne réseau, une actualisation de la page ou une fermeture du navigateur.

Ceci fait de **Basthon** un [outil à part](#others).

## Pourquoi ne pas utiliser des projets existants ? {#others}

Pour l'auteur, le critère le plus important est que l'outil doit pouvoir être utilisé par des élèves pour des activités d'enseignement. Il doit donc être :

 - respectueux de la vie-privée ;
 - conforme aux programmes (en France, les programmes issus de la réforme du lycée dite du Bac 2021 prescrivent l'utilisation de Python 3).

Le tableau comparatif suivant liste les différents projets testés par l'auteur à l'été 2020 pour interpréter du code Python. Il en ressort que **Basthon** est le seul outil répondant aux critères. C'est d'ailleurs de ce constat qu'est né **Basthon**.

|Projet|Pas de publicité|Pas d'analyse[¹](#footnote1)|Pas d'inscription|Exécution côté client|Python 3|
|----|----|----|----|----|----|
|[Repl.it](https://repl.it/languages/Python3)|✅|❌|✅|❌|✅|
|[Trinket](https://trinket.io/python)|✅|❌|✅|✅|❌|
|[Trinket (Python3)](https://trinket.io/features/python3)|✅|❌|✅|❌|✅|
|[CodeSkulptor](http://www.codeskulptor.org/)|✅|❌|✅|✅|❌|
|[CodeSkulptor3](https://py3.codeskulptor.org/)|✅|❌|✅|✅|〜|
|[Cocalc](https://cocalc.com/)|✅|❌|✅|❌|✅|
|[PythonSandbox](http://pythonsandbox.com/)|✅|❌|✅|✅|❌|
|[Lelivrescolaire.fr](https://www.lelivrescolaire.fr/outils/console-python)|✅|❌|✅|❌|✅|
|[OlineGDB](https://www.onlinegdb.com/online_python_interpreter)|❌|❌|✅|❌|✅|
|[Programiz](https://www.programiz.com/python-programming/online-compiler/)|❌|❌|✅|❌|✅|
|[Basthon](https://basthon.fr)|✅|✅|✅|✅|✅|

###### [1] Par analyse, on entend une analyse d'audience de type [Google Analytics](https://fr.wikipedia.org/wiki/Google_Analytics). {#footnote1}

On remarque que si l'on fait abstraction de la présence d'une analyse de fréquentation pour [CodeSkulptor3](https://py3.codeskulptor.org/), c'est une alternative envisageable à **Basthon**. Cependant, deux points sont à souligner :

 - le support de Python3 *n'est pas complet* (pour s'en convaincre, on peut par exemple comparer les attributs d'un `int` : `len(dir(1))` retourne 70 avec Python 3.7.4 et 45 avec Skulpt)
 - l'évaluation d'expressions [n'est pas supportée](https://github.com/skulpt/skulpt/issues/787) (la fonction `eval` n'est pas implémentée et, par exemple, aucune console Python utilisant Skulpt ne sait évaluer correctement l'expression `a = 1 ; 'truc' ; 55`).

Ce dernier point est rédhibitoire pour une utilisation de type notebook !

## Pourquoi ce choix d'interprète ? {#interpret}

Au début du projet, Basthon utilisait [Brython](https://brython.info/). Plusieurs bogues et régressions on fait que l'auteur s'est tourné vers [Skulpt](https://skulpt.org/). Malheureusement, Skulpt ne supporte pas complètement Python3 et surtout ne permet pas d'évaluer une expression Python ce qui empêche de l'utiliser dans un notebook. C'est alors que [Pyodide](https://github.com/iodide-project/pyodide) est devenu l'interprète de Bashon. Il est beaucoup plus lourd que les deux autres mais son support de Python est bien plus complet.

## Existe-t-il des bogues connus ? {#bugs}

Basthon est compatible avec les navigateurs Firefox, Chrome/Chromium et Edge.
      
Depuis l'utilisation de l'interpréteur Pyodide à la place de Brython, aucun problème n'a été recensé.

Si vous rencontrez un problème, merci de contacter l'auteur à l'adresse ci-dessous.
        
## Comment installer Basthon sur son propre serveur Web ? {#install}

C'est assez simple, il suffit de télécharger l'archive et de l'extraire à l'endroit approprié pour votre serveur Web.

  - Pour **Basthon-Console**, l'archive au format `tgz` se trouve [ici](https://console.basthon.fr/basthon-console.tgz).
  - Pour **Basthon-Notebook**, l'archive au format `tgz` se trouve [ici](https://notebook.basthon.fr/basthon-notebook.tgz).

## Comment contacter l'auteur de ce site ? {#author}
        
Vous pouvez contacter l'auteur à l'adresse suivante, `Romain.Casati_at_no_spam_ac-orleans-tours.fr`. Remplacer `_at_no_spam_` par `@`.
