const gulp = require('gulp'),
      ts = require("gulp-typescript"),
      merge = require('merge-stream');

/**
 * Copying style.css
 */
gulp.task('css', function() {
    return gulp.src('./src/style.css')
        .pipe(gulp.dest('lib/'));
});

/**
 * Compiling kernel.
 */
gulp.task('typescript', function () {
    const tsProject = ts.createProject("tsconfig.json");
    const tsResult = tsProject.src().pipe(tsProject());
    return merge(tsResult, tsResult.js)
        .pipe(gulp.dest('lib/'));
});

gulp.task('all', gulp.series('css', 'typescript'));
