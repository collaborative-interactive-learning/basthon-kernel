"""
Package (or module) loader.

It manage Pyodide and Basthon's modules load.
"""

import pyodide
import js
from . import _patch_modules
from .utils import OrderedSet


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["find_imports", "load_and_patch"]


# Link to useful pyodide's dict
_import_name_to_package_name = js.pyodide._module.packages.import_name_to_package_name
_import_name_to_package_name = {k: _import_name_to_package_name[k]
                                for k in dir(_import_name_to_package_name)
                                if isinstance(_import_name_to_package_name[k], str) and '.' not in k}

# Available packages in Pyodide.
_pyodide_pkgs = set(_import_name_to_package_name.keys())

# Root path to internal modules
_internal_modules_path = js.Basthon.basthonModulesRoot(True)

# Packages not implemented in Pyodide but in Basthon (internal)
# (dict pointing to Pypi or internal addresse).
_internal_pkgs_dict = {
    "turtle": {
        "path": f"{_internal_modules_path}/turtle-0.0.1-py3-none-any.whl",
    },
    "requests": {
        "path": f"{_internal_modules_path}/requests-0.0.1-py3-none-any.whl",
    },
    "proj4py": {
        "path": f"{_internal_modules_path}/proj4py-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
    "branca": {
        "path": "branca==0.4.2",  # loaded from PyPi
    },
    "folium": {
        "path": "folium==0.12.1",  # loaded from PyPi
        "deps": ["requests", "numpy", "branca"],
    },
    "graphviz": {
        "path": f"{_internal_modules_path}/graphviz-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
    "IPython": {
        "path": f"{_internal_modules_path}/IPython-0.0.1-py3-none-any.whl",
    },
    "p5": {
        "path": f"{_internal_modules_path}/p5-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
    "tutor": {
        "path": f"{_internal_modules_path}/tutor-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
    "_ivp": {
        "path": f"{_internal_modules_path}/ivp-0.0.1-py3-none-any.whl",
        "deps": ["numpy"],
    },
    "scipy": {  # this force scipy to load _ivp first
        "deps": ["_ivp", "numpy"],
    },
    "imageio": {  # this force imageio to load PIL first
        "deps": ["PIL"],
    },
    "wave": {  # this force wave to load audioop first
        "deps": ["audioop"],
    },
    "matplotlib": {  # this force numpy to be patched before matplotlib
        "deps": ["numpy"],
    },
    "pylab": {
        "deps": ["matplotlib"],
    },
    "qrcode": {
        "path": "qrcode==6.1",  # loaded from PyPi
        "deps": ["PIL"],  # to force patching
    },
    "osmiter": {
        "path": "osmiter==1.1.1",  # loaded from PyPi
    },
    "pyroutelib3": {
        "path": "pyroutelib3==1.7.1",  # loaded from PyPi
        "deps": ["osmiter", "requests"],
    },
    "ipythonblocks": {
        "path": f"{_internal_modules_path}/ipythonblocks-0.0.1-py3-none-any.whl",
        "deps": ["IPython", "PIL"],
    },
    "lolviz": {
        "path": f"{_internal_modules_path}/lolviz-0.0.1-py3-none-any.whl",
        "deps": ["graphviz"],
    },
    "binarytree": {
        "path": "https://files.pythonhosted.org/packages/df/01/0a932b539beb7fee36289f72fde5486ed49d73c0c24271a4eaab3e73084e/binarytree-6.3.0-py3-none-any.whl",  # loaded from PyPi (but without dependency check)
        "deps": ["graphviz"],
    },
    "rcviz": {
        "path": f"{_internal_modules_path}/rcviz-0.0.1-py3-none-any.whl",
        "deps": ["graphviz"],
    },
}

# checking that 'path' is in dict allows to add dependencies to pyodide packages
# (see _ivp/scipy)
_internal_pkgs = set(k for k, v in _internal_pkgs_dict.items() if 'path' in v)

# Union of internal and pyodide packages.
_all_pkgs = _pyodide_pkgs | _internal_pkgs

# Packages already loaded.
_loaded = set()


def _load_pyodide(packages):
    """ Load Pyodide packages (async). """
    if isinstance(packages, str):
        packages = [packages]

    if not packages:
        return js.Promise.resolve()
    # from Python name to Pyodide name
    return js.pyodide.loadPackage([_import_name_to_package_name[p]
                                   for p in packages])


def _load_internal(packages):
    """ Load internal module with micropip (async). """
    if isinstance(packages, str):
        packages = [packages]

    if not packages:
        return js.Promise.resolve()

    def micropip_wrapper(*args):
        return micropip_install([_internal_pkgs_dict[p]['path']
                                 for p in packages])

    return load_and_patch('micropip').then(micropip_wrapper)


def _internal_dependencies(packages):
    """
    Return the (ordered) set of dependencies of a list of internal packages.

    Order maters: last elements should be loaded first.
    """
    def dependencies(package):
        deps = OrderedSet(_internal_pkgs_dict.get(package, {}).get("deps", []))
        return deps.union(*(dependencies(p) for p in deps))

    packages = OrderedSet(packages)
    return OrderedSet.union(*(dependencies(p) for p in packages)) - packages


def load_and_patch(packages):
    """ Load and patch modules (internal or Pyodide). """
    if isinstance(packages, str):
        packages = [packages]

    # remove already loaded
    packages = OrderedSet(packages) - _loaded

    if not packages:
        return js.Promise.resolve()

    # last dependencies first to ensure deps are resolved when patching
    # the order is guarenteed by our OrderedSet
    packages = reversed(packages | _internal_dependencies(packages))
    pyodide_packages = packages & _pyodide_pkgs
    internal_packages = packages & _internal_pkgs

    def callback(*args):
        if packages:
            _loaded.update(packages)
            _patch_modules.patch(packages)

    return getattr(js.Promise.all((_load_pyodide(pyodide_packages),
                                   _load_internal(internal_packages))),
                   'finally')(callback)


def find_imports(code):
    """
    Wrapper around pyodide.find_imports.
    """
    if not isinstance(code, str):
        try:
            code = code.tobytes()
        except Exception:
            pass
        code = code.decode("utf-8-sig")
    return pyodide.find_imports(code)


def micropip_install(packages):
    """ Load packages using micropip and return a promise. """
    import micropip
    return micropip.install(packages)
