import pydoc
import builtins
import sys
from js import window
from . import packages
from pathlib import Path


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ['patch_input', 'patch_help', 'patch_six']


def patch_input():
    """ Patching Pyodide bugy input function. """
    _default_input = builtins.input

    def _patched_input(prompt=None):
        if prompt is not None:
            print(prompt, end='', flush=True)
        res = window.prompt(prompt)
        print(res)
        return res

    # copying all writable attributes (usefull to keep docstring and name)
    for a in dir(_default_input):
        try:
            setattr(_patched_input, a, getattr(_default_input, a))
        except Exception:
            pass

    # replacing
    builtins.input = _patched_input


def patch_help():
    """ Patching help function.

    See pydoc.py in cpython:
    https://github.com/python/cpython/blob/master/Lib/pydoc.py
    it uses a class called ModuleScanner to list packages.
    this class first looks at sys.builtin_module_names then in pkgutil.
    we fake sys.builtin_module_names in order to get it right
    """
    _default_help = pydoc.help

    def _patched_help(*args, **kwargs):
        backup = sys.builtin_module_names
        to_add = list(packages._all_pkgs)
        to_add.append('js')
        sys.builtin_module_names = backup + tuple(to_add)
        res = _default_help(*args, **kwargs)
        sys.builtin_module_names = backup
        return res

    pydoc.help = _patched_help


def patch_six():
    """
    In pyodide 0.16.1, six is installed under
    /lib/python3.8/site-packages/six.py
    when importing qrcode from PyPi (using micropip), it tries to install
    six (from PyPi too) under the same path which results in I/O error.

    To fix this, we move six.py under /lib/python3.8/
    """
    path = Path('/lib/python3.8/site-packages/six.py')
    assert path.exists(), "If you see this, patch_six should be useless."
    path.rename('/lib/python3.8/six.py')


def patch_all():
    """
    Patch all builtins by calling all patch_* methods.
    """
    for name, method in globals().items():
        if name.startswith('patch_') and name != 'patch_all':
            method()
