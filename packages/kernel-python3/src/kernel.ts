import { KernelBase } from '@basthon/kernel-base';


declare global {
    interface Window {
        languagePluginUrl?: string;
        languagePluginLoader?: Promise<any>;
        pyodide?: any;
    }
}

/**
 * A Python kernel that satisfies Basthon's API.
 */
export class KernelPython3 extends KernelBase {

    /**
     * Where to find pyodide.js (private).
     */
    private _pyodideUrl = "https://cdn.jsdelivr.net/pyodide/v0.16.1/full/pyodide.js";
    private __kernel__: any = null;
    public pythonVersion: string = "";

    constructor(rootPath: string) { super(rootPath); }

    /**
     * Get the URL of Basthon modules dir.
     */
    public basthonModulesRoot(absolute: boolean = false) {
        return this.basthonRoot(absolute) + "/modules";
    }

    public language() { return "python3"; }
    public languageName() { return "Python 3"; }
    public moduleExts() { return ['py']; }

    /**
     * What to do when loaded (private).
     */
    async _onload() {
        const pyodide = window.pyodide;
        // get the version of Python from Python
        this.pythonVersion = pyodide.runPython("import platform ; platform.python_version()");
        // this is for avoiding "Unknown package 'basthon'" error
        // but can be removed  with 0.17.0 since it is fixed upstream
        const consoleErrorBck = console.error;
        console.error = () => { };
        await pyodide.loadPackage(this.basthonRoot(true) + "/basthon.js");
        console.error = consoleErrorBck;
        // importing basthon to get it's kernel
        await pyodide.runPythonAsync("import basthon as __basthon__");
        // kernel lookup
        this.__kernel__ = pyodide.globals.__basthon__.__kernel__;
        // removing basthon from global namespace
        await pyodide.runPythonAsync("del __basthon__");
    }

    /**
     * Start the Basthon kernel asynchronously.
     */
    public async launch() {
        /* testing if Pyodide is installed locally */
        try {
            const url = this.basthonRoot() + "/pyodide/pyodide.js";
            const response = await fetch(url, { method: "HEAD" });
            if (response.ok) this._pyodideUrl = url;
        } catch (e) { }

        // forcing Pyodide to look at the right location for other files
        window.languagePluginUrl = this._pyodideUrl.substr(0, this._pyodideUrl.lastIndexOf('/')) + '/';

        try {
            await KernelPython3.loadScript(this._pyodideUrl);
        } catch (error) {
            console.log(error);
            console.error("Can't load pyodide.js");
            throw error;
        }

        if (window.languagePluginLoader == null) {
            throw new Error("Can't load pyodide.js");
        }

        await window.languagePluginLoader.then(
            this._onload.bind(this),
            function() { throw new Error("Can't load Python from Pyodide"); });
    }

    /**
     * Basthon async code evaluation function.
     */
    public async evalAsync(
        code: string,
        outCallback: (_: string) => void,
        errCallback: (_: string) => void,
        data: any = null): Promise<any> {
        if (typeof outCallback === 'undefined') {
            outCallback = function(text) { console.log(text); };
        }
        if (typeof errCallback === 'undefined') {
            errCallback = function(text) { console.error(text); };
        }
        // loading dependencies are loaded by eval
        return await this.__kernel__.eval(code, outCallback, errCallback, data);
    }

    /**
     * Restart the kernel.
     */
    public restart() {
        return this.__kernel__.restart();
    }

    /**
     * Put a file on the local (emulated) filesystem.
     */
    public putFile(filename: string, content: ArrayBuffer) {
        this.__kernel__.put_file(filename, content);
    }

    /**
     * Put an importable module on the local (emulated) filesystem
     * and load dependencies.
     */
    public putModule(filename: string, content: ArrayBuffer) {
        return this.__kernel__.put_module(filename, content);
    }

    /**
     * List modules launched via putModule.
     */
    public userModules() {
        return this.__kernel__.user_modules();
    }

    /**
     * Download a file from the VFS.
     */
    public getFile(path: string) {
        return this.__kernel__.get_file(path);
    }

    /**
     * Download a user module file.
     */
    public getUserModuleFile(filename: string) {
        return this.__kernel__.get_user_module_file(filename);
    }

    /**
     * Is the source ready to be evaluated or want we more?
     * Usefull to set ps1/ps2 for teminal prompt.
     */
    public more(source: string) {
        return this.__kernel__.more(source);
    }

    /**
     * Mimic the CPython's REPL banner.
     */
    public banner() {
        /* We don't return this.__kernel__.banner();
         * since the banner should be available ASAP.
         * In tests, we check this.banner() ===  this.__kernel__.banner().
         */
        return `Python 3.8.2 (default, Dec 25 2020 21:20:57) on WebAssembly VM\nType "help", "copyright", "credits" or "license" for more information.`;
    }

    /**
     * Complete a code at the end (usefull for tab completion).
     *
     * Returns an array of two elements: the list of completions
     * and the start index.
     */
    public complete(code: string) {
        return this.__kernel__.complete(code);
    }

    /**
     * Change current directory (Python's virtual FS).
     */
    public chdir(path: string) {
        window.pyodide._module.FS.chdir(path);
    }

    /**
     * Create directory (Python's virtual FS).
     */
    public mkdir(path: string) {
        window.pyodide._module.FS.mkdir(path);
    }
}
