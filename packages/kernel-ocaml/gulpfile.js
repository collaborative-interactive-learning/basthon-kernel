const gulp = require('gulp'),
      ts = require("gulp-typescript"),
      merge = require('merge-stream'),
      terser = require('gulp-terser');

/**
 * Compiling kernel.
 */
gulp.task('typescript', function () {
    const tsProject = ts.createProject("tsconfig.json");
    const tsResult = tsProject.src().pipe(tsProject());
    return merge(tsResult, tsResult.js)
        .pipe(gulp.dest('lib/'));
});

/**
 * Copying __kernel__.js.
 */
gulp.task('__kernel__', function () {
    return gulp.src('./src/__kernel__.js')
        .pipe(terser())
        .pipe(gulp.dest('lib/'));
});

gulp.task('all', gulp.parallel('typescript', '__kernel__'));
