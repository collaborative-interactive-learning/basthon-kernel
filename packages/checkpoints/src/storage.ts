import localForage from "localforage";

/**
 * A class to manage key/value browser storage.
 *
 * name is a string used to identify each storage instance.
 */
export class Storage<T> {
    private _ready: boolean = false;
    public ready = localForage.ready.bind(localForage);
    private readonly _store: LocalForage;
    private static readonly mainDB = "basthon";

    public constructor(name: string) {
        this._store = localForage.createInstance({
            name: Storage.mainDB,
            storeName: name,
        });

        this.ready().then(() => { this._ready = true; });
    }

    public async get(key: string): Promise<T | null | undefined> {
        if (this._ready)
            return await this._store.getItem(key);
    };

    public async set(key: string, value: T) {
        if (this._ready)
            await this._store.setItem(key, value);
    };

    public async remove(key: string) {
        if (this._ready)
            await this._store.removeItem(key);
    };

    public async clear() {
        if (this._ready)
            await this._store.clear();
    };

    public async length(): Promise<number> {
        if (this._ready)
            return await this._store.length();
        return 0;
    };

    public async keys(): Promise<string[] | undefined> {
        if (this._ready)
            return await this._store.keys();
    };

    public async iterate(callback: (a: T) => void) {
        if (this._ready)
            return await this._store.iterate(callback);
    };

    public static async drop(name: string) {
        await localForage.dropInstance({
            name: Storage.mainDB,
            storeName: name,
        });
    };
}
