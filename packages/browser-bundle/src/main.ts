import { KernelLoader } from "@basthon/kernel-loader";

declare global {
    interface Window {
        basthonRoot?: string;
        basthonKernel?: string;
    }
}

const loader = new KernelLoader(
    window.basthonRoot ?? "./",
    window.basthonKernel ?? "python3"
);

(async () => {
    loader.showLoader("Chargement de Basthon...", true);
    await loader.kernelLoaded();
})();
