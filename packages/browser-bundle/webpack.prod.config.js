const { merge } = require('webpack-merge');
const base = require('./webpack.base.config.js');
const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const pkg = require('./package.json');

let kernelVersion = pkg.version;
if (kernelVersion === "0.0.0")
    kernelVersion = require('../../lerna.json').version;


module.exports = merge(base, {
    mode: 'production',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(tsx?|jsx?)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets:
                        [
                            "@babel/preset-env",
                            "@babel/preset-typescript",
                        ],
                        plugins: [
                            "@babel/plugin-transform-runtime",
                        ],
                    },
                },
                // running babel on js_of_ocaml output breaks it
                exclude: /(node_modules|__kernel__\.js)/,
            },
        ]
    },
    plugins: [
        new ForkTsCheckerWebpackPlugin({
            typescript: {
                diagnosticOptions: {
                    semantic: true,
                    syntactic: true,
                },
            },
        }),
        new CopyPlugin({
            patterns: [
                { // python3 files
                    from: "**/*",
                    context: "../kernel-python3/lib/dist/",
                    to: path.resolve(__dirname, 'lib', kernelVersion),
                    toType: "dir"
                },
            ]
        }),
    ],
    optimization: {
        chunkIds: "named",
        minimize: true,  // useless
        minimizer: [
            new TerserPlugin({
                // js_of_ocaml output is heavy and already minified
                exclude: /kernel-ocaml_lib___kernel___js.js/,
            }),
        ],
      },
});
