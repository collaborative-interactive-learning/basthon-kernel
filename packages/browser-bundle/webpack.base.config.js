const path = require("path");

module.exports = {
    entry: "./src/main.ts",
    output: {
        filename: 'bundle.js',
        chunkFilename: '[name].js',
        path: path.join(__dirname, 'lib'),
        clean: true,
    },
    module: {
        rules: [
            {   // to ignore css import in kernel-loader
                test: /\.css$/,
                use: "ignore-loader"
            },
            {
                resourceQuery: /asset-url/,
                type: 'asset/resource',
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js'],
        fallback: {  // for ocaml bundle
            "constants": require.resolve("constants-browserify"),
            "tty": require.resolve("tty-browserify"),
            "fs": false,
            "child_process": false,
            // for sql bundle
            "crypto": require.resolve("crypto-browserify"),
            "path": require.resolve("path-browserify"),
            "buffer": require.resolve("buffer/"),
            "stream": require.resolve("stream-browserify"),
        }
    },
};
