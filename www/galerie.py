examples = {
    "Python": {
        "courbes.py": {
            'desc': "Tracé de courbes avec Matplotlib",
        },
        "project-euler.py": {
            'desc': "Un problème du <a href='https://projecteuler.net/'>Projet Euler</a>",
        },
        "spirale.py": {
            'desc': "Une spirale avec Turtle",
        },
        "3d-plot.ipynb": {
            'desc': "Tracé de surface avec Matplotlib",
        },
        "sklearn.ipynb": {
            'desc': "Classification avec Scikit-Learn",
        },
        "sympy.ipynb": {
            'desc': "Calcul formel avec Sympy",
        },
        "decorateurs.ipynb": {
            'desc': "Fractales, Turtle et décorateurs",
        },
        "pypi.ipynb": {
            'desc': "Chargement d'un module PyPi",
        },
        "easter-eggs.ipynb": {
            'desc': "Quelques easter eggs de (C)Python",
        },
        "markdown.ipynb": {
            'desc': "Utilisation de Markdown dans un notebook",
        },
        "folium.py": {
            'desc': "Des cartes avec Folium",
        },
        "folium-notebook.ipynb": {
            'desc': "Folium dans un notebook",
        },
        "linear-regression.py": {
            'desc': "Régression linéaire",
        },
        "lorenz.py": {
            'desc': "Attracteurs de Lorenz",
        },
        "requests.py": {
            'desc': "Requêtes HTTP avec requests",
        },
        "dom.ipynb": {
            'desc': "Modification du DOM",
        },
        "pandas.ipynb": {
            'desc': "Utilisation de Pandas",
        },
        "osm-process.ipynb": {
            'desc': "Récupération et traitement des données OpenStreetMap de la France",
        },
        "contour-france.py": {
            'desc': "Contour de la France continentale (OSM)",
        },
        "centre-france.ipynb": {
            'desc': "Où est le centre de la France ?",
        },
        "turtle-writing.ipynb": {
            'desc': "Une tortue qui sait écrire",
        },
        "lists.ipynb": {
            'desc': "Des listes chaînées bien typées",
        },
        "graphviz-console.py": {
            'desc': "Graphviz dans la console",
        },
        "graphviz-notebook.ipynb": {
            'desc': "Graphviz dans un notebook",
        },
        "traitement-image.ipynb": {
            'desc': "Traitement d'image avec Numpy et Matplotlib",
            'aux': "examples/sandbox.png",
        },
        "snake.ipynb": {
            'desc': "Traitement d'image avancé avec Scipy",
            'aux': "examples/snake.png"
        },
        "p5-square.py": {
            'desc': "Un exemple simple d'utilisation de p5",
        },
        "p5-basthon-on-fire.py": {
            'desc': "p5 enflamme Basthon",
        },
        "p5-torus.py": {
            'desc': "Animation 3d avec p5",
        },
        "p5-tree.py": {
            'desc': "Un arbre récursif avec p5",
        },
        "p5-waves.py": {
            'desc': "De drôles de bestioles avec p5",
        },
        "p5-camera.py": {
            'desc': "Utilisation de la webcam avec p5",
        },
        "p5.ipynb": {
            'desc': "Utilisation de p5 dans un notebook",
        },
        "p5-balls.ipynb": {
            'desc': "Des balles qui rebondissent avec p5",
        },
        "p5-instance.ipynb": {
            'desc': "Plusieurs dessins avec p5 en mode instance",
        },
        "slideshow.ipynb": {
            'desc': "Un diaporama avec un notebook (RISE) "
        },
        "moon.ipynb": {
            'desc': "Manipulation d'images avec PIL",
            'aux': "examples/moon.jpg",
        },
        "pythontutor-variables.py": {
            'desc': "Visualisation d'exécution avec PythonTutor (Console)",
        },
        "pythontutor.ipynb": {
            'desc': "Visualisation d'exécution avec PythonTutor (Notebook)",
        },
        "lotka-volterra.ipynb": {
            'desc': "Intégration d'EDO avec Scipy",
        },
        "qrcode.ipynb": {
            'desc': "Des codes QR dans le notebook",
        },
        "qrcode-strange.py": {
            'desc': "Un drôle de code QR...",
        },
        "pyroutelib3.ipynb": {
            'desc': "Un calcul d'intinéraire avec Pyroutelib3",
        },
        "chute-libre-simple.py": {
            'desc': "Étude simple d'une chute libre",
        },
        "chute-libre.ipynb": {
            'desc': "Étude avancée d'une chute libre",
        },
        "ipythonblocks.ipynb": {
            'desc': "Apprendre avec ipythonblocks",
        },
        "lolviz.ipynb": {
            'desc': "Visualiser des structures de données avec lolviz",
        },
        "binarytree.ipynb": {
            'desc': "Manipuler et visualiser des arbres binaires avec binarytree",
        },
        "rcviz.ipynb": {
            'desc': "Visualiser des appels récursifs avec rcviz",
        },
        "matplotlib-animation.ipynb": {
            'desc': "Des animations avec Matplotlib",
        },
        "matplotlib-animation-console.py": {
            'desc': "Des animations Matplotlib dans la console",
        },
        "audio.ipynb": {
            'desc': "Du son dans un notebook",
        },
    },
    "SQL": {
        "voitures.ipynb": {
            'desc': "Chargement d'une base depuis un fichier",
            'module': "examples/voitures.sql",
        },
        "foreign-key.ipynb": {
            'desc': "Une clef étrangère",
        },
        "export-sql.ipynb": {
            "desc": "Export binaire d'une base",
        },
        "dot-commands-sql.ipynb": {
            "desc": "Commandes spéciales (dot commands)",
        }
    },
    "OCaml": {
        "factorial-ocaml.ml": {
            'desc': "Fonction factorielle",
        },
        "fichiers-ocaml.ipynb": {
            'desc': "Manipulation de fichiers texte",
            'aux': "examples/lorem-ipsum.txt",
        },
        "hanoi-ocaml.ml": {
            'desc': "Tours de Hanoï",
        },
        "graphics-ocaml.ipynb": {
            'desc': "Utilisation du module Graphics",
        },
        "pong-ocaml.ml": {
            'desc': "Animation sur un canvas",
        },
        "module-externe-ocaml.ipynb": {
            'desc': "Charger ses propres modules",
            'module': "examples/primes.ml",
        },
        "fichiers-annexes-ocaml.ml": {
            'desc': "Joindre des fichiers annexes",
            'aux': "examples/basthon.png",
        },
        "files-ocaml.ipynb": {
            'desc': "Écrire, lire et télécharger des fichiers",
        },
    },
    "JavaScript": {
    },
}
